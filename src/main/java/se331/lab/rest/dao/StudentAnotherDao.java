package se331.lab.rest.dao;

import se331.lab.rest.entity.Student;

import java.util.List;

public interface StudentAnotherDao {
    Student getStudentByName(String name);
    List<Student> getAllStudent();
}
