package se331.lab.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.StudentAnotherDao;
import se331.lab.rest.entity.Student;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentAnotherServiceImpl implements StudentAnotherService {

    @Autowired
    StudentAnotherDao studentAnotherDao;
    @Override
    public List<Student> getStudentByNameContains(String contain) {
        List<Student> students = studentAnotherDao.getAllStudent();
        List<Student> output = new ArrayList<>();
        for(Student student : students){
            if(student.getName().toLowerCase().contains(contain)){
                output.add(student);

            }
        }
        return  output;

    }

    @Override
    public List<Student> getStudentWhoseAdvisorNameIs(String lec) {
        List<Student> students = studentAnotherDao.getAllStudent();
        List<Student> output = new ArrayList<>();
        for (Student student:
                students) {
            String advisorName = student.getAdvisor().getName();
            if (advisorName.equalsIgnoreCase(lec)){
                output.add(student);
            }

        }
        return output;
    }

}



