package se331.lab.rest.service;

import org.springframework.stereotype.Service;
import se331.lab.rest.entity.Student;

import java.util.List;

@Service
public interface StudentAnotherService {
    List<Student> getStudentByNameContains(String contain);
    List<Student> getStudentWhoseAdvisorNameIs(String lec);
}
