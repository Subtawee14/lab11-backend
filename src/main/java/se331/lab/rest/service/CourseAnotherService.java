package se331.lab.rest.service;

import org.springframework.stereotype.Service;
import se331.lab.rest.entity.Course;

import java.util.List;

@Service
public interface CourseAnotherService {
    List<Course> CourseWhichStudentEnrolledMoreThan(int num);
}
