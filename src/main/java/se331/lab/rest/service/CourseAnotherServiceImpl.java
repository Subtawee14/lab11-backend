package se331.lab.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.CourseDao;
import se331.lab.rest.dao.StudentAnotherDao;
import se331.lab.rest.entity.Course;
import se331.lab.rest.entity.Student;

import java.util.ArrayList;
import java.util.List;

@Service
public class CourseAnotherServiceImpl implements CourseAnotherService {
    @Autowired
    CourseDao courseDao;

    @Override
    public List<Course> CourseWhichStudentEnrolledMoreThan(int num) {
        List<Course> course = courseDao.getAllCourses();
        List<Course> output = new ArrayList<>();
        for(Course i : course){
            if(i.getStudents().size() > num){
                output.add(i);
            }
        }
        return  output;
    }
}
