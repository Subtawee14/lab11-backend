package se331.lab.rest.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import se331.lab.rest.entity.Student;
import se331.lab.rest.mapper.MapperUtil;
import se331.lab.rest.service.LecturerAnotherService;
import se331.lab.rest.service.StudentAnotherService;
import se331.lab.rest.service.StudentService;

@Controller
@Slf4j
public class StudentAnotherController {

    @Autowired
    StudentAnotherService studentService;

    @GetMapping("/getStudentByNameContains/{name}")
    public ResponseEntity getStudentByName(@PathVariable String name) {
        log.info("Search student name/{name}");
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentDto(this.studentService.getStudentByNameContains(name)));
    }
    @GetMapping("/StudentWhoseAdvisorNameIs/{name}")
    public ResponseEntity getStudentWhoseAdvisorNameIs(@PathVariable String name) {
        log.info("Search advisorname / {name}");
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentDto(this.studentService.getStudentWhoseAdvisorNameIs(name)));
    }



}